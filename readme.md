#### Prerequisites
  - Java 8
  - Maven
  - TestNG
  - Selenium
  - Geckodriver included into system PATH
  - Firefox browser installed
  
#### Getting started
  - clone the project
  - in the terminal go to ```check24``` (project root) directory
  - run ```mvn install```
  - run tests ```mvn test```