package config;

import com.github.javafaker.Faker;
import org.apache.commons.lang3.time.DateUtils;

import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.Locale;

public class Config {

    public interface App {
        String baseUrl = "https://finanzen.check24.de";
    }

    public interface User {
        Faker faker = new Faker(new Locale("de_DE"));

        String firstName = faker.name().firstName();
        String lastName = faker.name().lastName();
        SimpleDateFormat sm = new SimpleDateFormat("dd-MM-yyyy");
        String dateOfBirth = sm.format(faker.date().between(DateUtils.addYears(new Date(), -80), DateUtils.addYears(new Date(), -18)));
        String placeOfBirth = "München";
        String maritalStatus = "ledig";
        String nationality = "Deutschland";

        String postCode = faker.address().zipCode();
        String city = "80636 München";
        String streetName = faker.address().streetName();
        String houseNumber = faker.address().buildingNumber();
        String housingSituation = "zur Miete";

        String phoneNumber = "+49176" + faker.number().digits(7);
        String email = faker.internet().emailAddress();

//        SimpleDateFormat sm = new SimpleDateFormat("dd-MM-yyyy");
//        String strDate = sm.format(faker.date().between(DateUtils.addYears(new Date(), -80), DateUtils.addYears(new Date(), -18)));
    }

}
