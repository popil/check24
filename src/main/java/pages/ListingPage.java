package pages;

import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import pages.elements.BaseButton;

import static tools.WebDriverMgr.getDriver;


public class ListingPage extends BasePage {
    public static class ContinueButton extends BaseButton<CustomerDetailsPage> {
        ContinueButton(String orderNo) {
            super(new By.ByXPath(String.format("//div[@id=\"result-list\"]//div[@class=\"product-panel__title\"][span[@class=\"product-panel__title__counter\" and .=\"%s.\"]]/following-sibling::div/div[@class=\"product-panel__controls__button\"]/a", orderNo)));
        }

        @Override
        public CustomerDetailsPage goTo() {
            return new CustomerDetailsPage(getDriver());
        }
    }

    public ListingPage(WebDriver driver) {
        super(driver);
    }

    public ContinueButton getContinueButtonByProductOrderNo(String orderNo) {
        return new ContinueButton(orderNo);
    }
}


