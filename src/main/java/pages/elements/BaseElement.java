package pages.elements;

import org.openqa.selenium.By;
import org.openqa.selenium.ElementNotVisibleException;
import org.openqa.selenium.NoSuchElementException;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.support.ui.WebDriverWait;

import static tools.WebDriverMgr.getDriver;

public abstract class BaseElement<T> {
    private Integer TIMEOUT = 30;
    private WebDriverWait wait = new WebDriverWait(getDriver(), TIMEOUT);
    private final By locator;

    public BaseElement(By locator) {
        this.locator = locator;
    }

    protected T goTo() {
        return (T)this;
    }

    protected By getLocator() {
        return locator;
    }

    protected WebElement findElement() {
        return getDriver().findElement(getLocator());
    }

    protected WebElement waitForClickableElement() throws NoSuchElementException {
        return wait.until(ExpectedConditions.elementToBeClickable(getLocator()));
    }

    public boolean isDisplayed() {
        try {
            findElement().isEnabled();
            return true;
        } catch (NoSuchElementException | ElementNotVisibleException e) {
            return false;
        }
    }
}