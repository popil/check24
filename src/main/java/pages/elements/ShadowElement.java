//package pages.elements;
//
//import io.github.sukgu.Shadow;
//import org.openqa.selenium.By;
//import org.openqa.selenium.WebElement;
//
//import static tools.WebDriverMgr.getDriver;
//
//public abstract class ShadowElement<PAGE> extends BaseElement<PAGE> {
//    public ShadowElement(By locator) {
//        super(locator);
//    }
//
//    @Override
//    protected WebElement findElement() {
//        Shadow shadow = new Shadow(getDriver());
//        return shadow.findElement(getLocator().toString().replace("By.cssSelector: ", ""));
//    }
//
//    public PAGE click() {
//        findElement().click();
//        return goTo();
//    }
//
//    public void sendText(String text) {
//        WebElement elm = findElement();
//        elm.clear();
//        elm.sendKeys(text);
//    }
//}