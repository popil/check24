package pages.elements;

import org.openqa.selenium.By;

public abstract class BaseCheckBox extends BaseButton {
    public BaseCheckBox(By locator) {
        super(locator);
    }
}
