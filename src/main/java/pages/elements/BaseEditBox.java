package pages.elements;

import org.openqa.selenium.By;
import org.openqa.selenium.Keys;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.interactions.Actions;

import static tools.WebDriverMgr.getDriver;

public abstract class BaseEditBox extends BaseElement {
    public BaseEditBox(By locator) {
        super(locator);
    }

    public void sendText(String text) {
        WebElement elm = waitForClickableElement();
        elm.clear();
        elm.sendKeys(text);
    }

    public void clear() {
        waitForClickableElement().clear();
    }

}