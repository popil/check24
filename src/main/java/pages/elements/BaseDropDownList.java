package pages.elements;

import org.openqa.selenium.By;
import org.openqa.selenium.support.ui.Select;


public abstract class BaseDropDownList extends BaseButton {
    public BaseDropDownList(By locator) {
        super(locator);
    }

    public void select(String text) {
        Select dropdown = new Select(waitForClickableElement());
        dropdown.selectByVisibleText(text);
    }
}
