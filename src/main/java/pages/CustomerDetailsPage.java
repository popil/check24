package pages;

import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import pages.elements.BaseButton;
import pages.elements.BaseCheckBox;
import pages.elements.BaseDropDownList;
import pages.elements.BaseEditBox;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import static tools.WebDriverMgr.getDriver;


public class CustomerDetailsPage extends BasePage {

    public static class EmailEditBox extends BaseEditBox {
        EmailEditBox() {
            super(new By.ById("cl_login"));
        }
    }

    public static class ContinueButton extends BaseButton {
        ContinueButton() {
            super(new By.ById("c24-uli-login-btn"));
        }
    }

    public static class ContinueAsGuestLink extends BaseButton {
        ContinueAsGuestLink() {
            super(new By.ByCssSelector(".c24-uli-bottom-l-anonymous > a"));
        }
    }

    public static class ContinueAsGuestRadioButton extends BaseButton {
        ContinueAsGuestRadioButton() {
            super(new By.ByCssSelector(".c24-uli-cl-r-anonym .c24-uli-cl-box-option"));
        }
    }

    public static class ProceedToRegistrationButton extends BaseButton {
        ProceedToRegistrationButton() {
            super(new By.ById("c24-uli-register-btn"));
        }
    }

    public static class FemaleCheckBox extends BaseCheckBox implements CustomerDetails {
        FemaleCheckBox() {
            super(new By.ByXPath("//div[input[@id=\"GENDER_FEMALE\"]]"));
        }

        public boolean isErrorDisplayed() {
            return waitForClickableElement().findElement(new By.ByXPath("./../../../../div/following-sibling::div[@type=\"ERROR\"]")).isDisplayed();
        }
    }

    public static class MaleCheckBox extends BaseCheckBox {
        MaleCheckBox() {
            super(new By.ByXPath("//div[input[@id=\"GENDER_MALE\"]]"));
        }
    }

    public static class GivenNameEditBox extends BaseEditBox implements CustomerDetails {
        GivenNameEditBox() {
            super(new By.ById("GIVEN_NAME"));
        }

        public boolean isErrorDisplayed() {
            return waitForClickableElement().findElement(new By.ByXPath("./../../following-sibling::div[@type=\"ERROR\"][1]")).isDisplayed();
        }
    }

    public static class LastNameEditBox extends BaseEditBox implements CustomerDetails {
        LastNameEditBox() {
            super(new By.ById("LAST_NAME"));
        }

        public boolean isErrorDisplayed() {
            return waitForClickableElement().findElement(new By.ByXPath("./../../following-sibling::div[@type=\"ERROR\"][2]")).isDisplayed();
        }
    }

    public static class DateOfBirthEditBox extends BaseEditBox implements CustomerDetails {
        DateOfBirthEditBox() {
            super(new By.ById("DATE_OF_BIRTH"));
        }

        public boolean isErrorDisplayed() {
            return waitForClickableElement().findElement(new By.ByXPath("./../../following-sibling::div[@type=\"ERROR\"]")).isDisplayed();
        }
    }

    public static class PlaceOfBirthEditBox extends BaseEditBox implements CustomerDetails {
        PlaceOfBirthEditBox() {
            super(new By.ById("PLACE_OF_BIRTH"));
        }

        public boolean isErrorDisplayed() {
            return waitForClickableElement().findElement(new By.ByXPath("./../../following-sibling::div[@type=\"ERROR\"]")).isDisplayed();
        }
    }

    public static class MaritalStatusDropDownList extends BaseDropDownList implements CustomerDetails {
        MaritalStatusDropDownList() {
            super(new By.ById("MARITAL_STATUS"));
        }

        public boolean isErrorDisplayed() {
            return waitForClickableElement().findElement(new By.ByXPath("./../../following-sibling::div[@type=\"ERROR\"]")).isDisplayed();
        }
    }

    public static class NationalityDropDownList extends BaseDropDownList implements CustomerDetails {
        NationalityDropDownList() {
            super(new By.ById("NATIONALITY"));
        }

        public boolean isErrorDisplayed() {
            return waitForClickableElement().findElement(new By.ByXPath("./../../following-sibling::div[@type=\"ERROR\"]")).isDisplayed();
        }
    }

    public static class PostalCodeEditBox extends BaseEditBox implements CustomerDetails {
        PostalCodeEditBox() {
            super(new By.ById("POSTAL_CODE"));
        }

        public boolean isErrorDisplayed() {
            return waitForClickableElement().findElement(new By.ByXPath("./../../following-sibling::div[@type=\"ERROR\"]")).isDisplayed();
        }
    }

    public static class StreetNameEditBox extends BaseEditBox implements CustomerDetails {
        StreetNameEditBox() {
            super(new By.ByName("STREET"));
        }

        public boolean isErrorDisplayed() {
            return waitForClickableElement().findElement(new By.ByXPath("./../../../following-sibling::div[@type=\"ERROR\"][1]")).isDisplayed();
        }
    }

    public static class HouseNumberEditBox extends BaseEditBox implements CustomerDetails {
        HouseNumberEditBox() {
            super(new By.ById("HOUSE_NUMBER"));
        }

        public boolean isErrorDisplayed() {
            return waitForClickableElement().findElement(new By.ByXPath("./../../following-sibling::div[@type=\"ERROR\"][2]")).isDisplayed();
        }
    }

    public static class HousingSituationDropDownList extends BaseDropDownList implements CustomerDetails {
        HousingSituationDropDownList() {
            super(new By.ById("HOUSING_SITUATION"));
        }

        public boolean isErrorDisplayed() {
            return waitForClickableElement().findElement(new By.ByXPath("./../../following-sibling::div[@type=\"ERROR\"]")).isDisplayed();
        }
    }

    public static class PhoneNumberEditBox extends BaseEditBox implements CustomerDetails {
        PhoneNumberEditBox() {
            super(new By.ById("PHONENUMBER_MOBILE"));
        }

        public boolean isErrorDisplayed() {
            return waitForClickableElement().findElement(new By.ByXPath("./../../following-sibling::div[@type=\"ERROR\"]")).isDisplayed();
        }
    }

    public static class ContactEmailEditBox extends BaseEditBox {
        ContactEmailEditBox() {
            super(new By.ById("CONTACT_EMAIL"));
        }
    }

    public static class NextButton extends BaseButton<ConfirmationPage> {
        NextButton() {
            super(new By.ByXPath("//div[@id=\"application-form\"]//a[text()=\"weiter\"]"));
        }

        @Override
        public ConfirmationPage goTo() {
            return new ConfirmationPage(getDriver());
        }
    }

    public CustomerDetailsPage(WebDriver driver) {
        super(driver);
    }

    public EmailEditBox emailEditBox = new EmailEditBox();
    public ContinueButton continueButton = new ContinueButton();
    public ContinueAsGuestLink continueAsGuestLink = new ContinueAsGuestLink();
    public ContinueAsGuestRadioButton continueAsGuestRadioButton = new ContinueAsGuestRadioButton();
    public ProceedToRegistrationButton proceedToRegistrationButton = new ProceedToRegistrationButton();

    public FemaleCheckBox femaleCheckBox = new FemaleCheckBox();
    public MaleCheckBox maleCheckBox = new MaleCheckBox();
    public GivenNameEditBox givenNameEditBox = new GivenNameEditBox();
    public LastNameEditBox lastNameEditBox = new LastNameEditBox();

    public DateOfBirthEditBox dateOfBirthEditBox = new DateOfBirthEditBox();
    public PlaceOfBirthEditBox placeOfBirthEditBox = new PlaceOfBirthEditBox();
    public MaritalStatusDropDownList maritalStatusDropDownList = new MaritalStatusDropDownList();
    public NationalityDropDownList nationalityDropDownList = new NationalityDropDownList();

    public PostalCodeEditBox postalCodeEditBox = new PostalCodeEditBox();
    public StreetNameEditBox streetEditBox = new StreetNameEditBox();
    public HouseNumberEditBox houseNumberEditBox = new HouseNumberEditBox();

    public HousingSituationDropDownList housingSituationDropDownList = new HousingSituationDropDownList();

    public PhoneNumberEditBox phoneNumberEditBox = new PhoneNumberEditBox();
    public ContactEmailEditBox contactEmailEditBox = new ContactEmailEditBox();

    public NextButton nextButton = new NextButton();

    protected interface CustomerDetails {
        boolean isErrorDisplayed();
    }

    public Map<String, Boolean> getDisplayedWarnings() {
        List<CustomerDetails> list = new ArrayList<>();
        list.add(femaleCheckBox);
        list.add(givenNameEditBox);
        list.add(lastNameEditBox);

        list.add(dateOfBirthEditBox);
        list.add(placeOfBirthEditBox);
        list.add(maritalStatusDropDownList);
        list.add(nationalityDropDownList);

        list.add(postalCodeEditBox);
        list.add(streetEditBox);
        list.add(houseNumberEditBox);
        list.add(housingSituationDropDownList);
        list.add(phoneNumberEditBox);

        Map<String, Boolean> warnings = new HashMap<>();

        for (CustomerDetails element : list) {
            warnings.put(element.getClass().getSimpleName(), element.isErrorDisplayed());
        }
        return warnings;
    }
}
