package pages;

import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import pages.elements.BaseCheckBox;

public class ConfirmationPage extends BasePage {

    public static class AcceptTermsAndConditionsCheckBox extends BaseCheckBox {
        AcceptTermsAndConditionsCheckBox() {
            super(new By.ByXPath("//label[input[@id=\"ACCEPT_TERMS_AND_CONDITIONS_PRODUCT\"]]"));
        }
    }


    public ConfirmationPage(WebDriver driver) {
        super(driver);
    }

    public AcceptTermsAndConditionsCheckBox acceptTermsAndConditionsCheckBox = new AcceptTermsAndConditionsCheckBox();

}
