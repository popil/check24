package web;

import org.testng.Assert;
import org.testng.annotations.Test;
import pages.ConfirmationPage;
import pages.CustomerDetailsPage;
import pages.ListingPage;
import config.Config.User;

import java.util.Map;

public class TestCreditCardComparison extends BaseWebTest {

    @Test
    public void testCreditCardComparisonFlow() {
        Assert.assertEquals(driver.manage().getCookieNamed("ppset").getValue(), "kreditkarte");

        ListingPage listingPage = new ListingPage(driver);
        CustomerDetailsPage customerDetailsPage = listingPage.getContinueButtonByProductOrderNo("1").click();
        customerDetailsPage.emailEditBox.sendText(User.email);
        customerDetailsPage.continueButton.click();

        customerDetailsPage.continueAsGuestRadioButton.click();
        customerDetailsPage.proceedToRegistrationButton.click();

        customerDetailsPage.nextButton.click();

        // verify warning appears on every unpopulated field
        for (Map.Entry<String, Boolean> item : customerDetailsPage.getDisplayedWarnings().entrySet()) {
            String fieldName = item.getKey();
            Boolean errorDisplayed = item.getValue();
            Assert.assertTrue(errorDisplayed, String.format("Warning did not appear on %s field", fieldName));
        }

        // populate customer details fields
        customerDetailsPage.maleCheckBox.click();
        customerDetailsPage.givenNameEditBox.sendText(User.firstName);
        customerDetailsPage.lastNameEditBox.sendText(User.lastName);

        customerDetailsPage.dateOfBirthEditBox.sendText(User.dateOfBirth);
        customerDetailsPage.placeOfBirthEditBox.sendText(User.placeOfBirth);
        customerDetailsPage.maritalStatusDropDownList.select(User.maritalStatus);
        customerDetailsPage.nationalityDropDownList.select(User.nationality);

        customerDetailsPage.postalCodeEditBox.sendText(User.city);
        customerDetailsPage.streetEditBox.sendText(User.streetName);
        customerDetailsPage.houseNumberEditBox.sendText(User.houseNumber);
        customerDetailsPage.housingSituationDropDownList.select(User.housingSituation);
        customerDetailsPage.phoneNumberEditBox.sendText(User.phoneNumber);

        ConfirmationPage confirmationPage = customerDetailsPage.nextButton.click();
        Assert.assertTrue(confirmationPage.acceptTermsAndConditionsCheckBox.isDisplayed());
    }
}
