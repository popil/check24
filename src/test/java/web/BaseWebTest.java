package web;

import config.Config;
import org.openqa.selenium.JavascriptExecutor;
import org.openqa.selenium.WebDriver;
import org.testng.annotations.AfterMethod;
import org.testng.annotations.BeforeMethod;
import tools.WebDriverMgr;

public class BaseWebTest {
    protected WebDriver driver;

    @BeforeMethod
    public void setUp() {
        driver = WebDriverMgr.getDriver();
        driver.manage().window().maximize();
        driver.get(Config.App.baseUrl + "/accounts/d/kreditkarte/result.html");
        acceptCookies();
    }

    private void acceptCookies() {
        ((JavascriptExecutor)driver).executeScript("Check24.cookieBanner.c24consent.giveConsent('fam')");
    }

    @AfterMethod
    public void tearDown() {
        driver.quit();
    }
}
