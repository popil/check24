package api;

import io.qameta.allure.Description;
import io.restassured.http.ContentType;
import org.testng.annotations.Test;

import static io.restassured.RestAssured.given;
import static io.restassured.module.jsv.JsonSchemaValidator.matchesJsonSchemaInClasspath;
import static org.hamcrest.CoreMatchers.containsString;


public class TestCreditCardAccount extends BaseApiTest {

    @Test
    @Description("Verify response with existing ID")
    public void testResponseWithValidId() {
        given().
        when().
            get("/accounts/r/frs/productInfo/kreditkarte/200007").
        then().
            statusCode(200).
            assertThat().body(matchesJsonSchemaInClasspath("schemas/creditCard.json"));
    }

    @Test
    @Description("Verify response with invalid ID")
    public void testResponseWithInvalidIdValue() {
        given().
        when().
            get("/accounts/r/frs/productInfo/kreditkarte/abcd").
        then().
            statusCode(404).
            assertThat().statusCode(404);
    }

    @Test
    @Description("Verify response with incorrect ID")
    public void testResponseWithIncorrectId() {
        given().
            contentType("application/json").
        when().
            get("/accounts/r/frs/productInfo/kreditkarte/5456").
        then().
            statusCode(204);
    }
}
