package api;

import io.qameta.allure.Step;
import io.qameta.allure.restassured.AllureRestAssured;
import io.restassured.RestAssured;
import io.restassured.http.ContentType;
import io.restassured.parsing.Parser;
import io.restassured.response.Response;
import io.restassured.specification.RequestSpecification;
import org.json.simple.JSONObject;
import org.json.simple.parser.JSONParser;
import org.json.simple.parser.ParseException;
import org.testng.annotations.BeforeClass;

import java.io.FileReader;
import java.io.IOException;
import java.io.Reader;


public class BaseApiTest {
    JSONObject userLoginPayload = null;
    Response loginSession;

    @BeforeClass
    public void setup() {
        RestAssured.baseURI = "https://finanzen.check24.de";
        RestAssured.defaultParser = Parser.JSON;
    }

    // wrap RestAssured given() with Allure report filter
    private RequestSpecification given() {
        return RestAssured.given().filter(new AllureRestAssured());
    }
}

